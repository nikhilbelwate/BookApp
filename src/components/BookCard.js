import React, { useState, useEffect } from 'react';
import './BookCard.css';
import axios from 'axios';
export default function BookCard({book}){

    const handleDelete=()=>{
        axios
      .delete("https://bookappbackend-gh0a.onrender.com" + book._id)
      .then((response) => {
        alert(response.data.message);
        window.location = '/';
      });
    }
    return (<div class="card-container">
    <img
      src="https://images.unsplash.com/photo-1495446815901-a7297e633e8d"
      alt="Books"
      height="200"
    />
    <div class="desc">
      <h2><a href="/show-book/123id">{book.title}</a></h2>
      <h3>{book.author}</h3>
      <p>{book.description}</p>
    </div>
    <button style={{"marginLeft":"85%"}} onClick={handleDelete}>X</button>
  </div>)
}