import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import BookList from './components/FrontPage_BookList'
import CreateBook from './components/CreateBook'

// import Junk from "./components/junk";

export default function App() {
  return (
    <>

    <BrowserRouter>
    <Routes>
    {/* <Navbar /> */}

      {/* <div className="container"> */}
        <Route path="/" exact element={<BookList />} />
        <Route path="/create-book" element={<CreateBook/>}/>
      {/* </div> */}
      </Routes>
    </BrowserRouter>
    </>
  );
}
