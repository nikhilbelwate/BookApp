import React, { useState, useEffect } from 'react';
import './BookCard.css';
import axios from 'axios';
export default function CreateBook(){

    const [title,setTitle]=useState();
    const [author,setAuthor]=useState();
    const [description,setDescription]=useState();

    const handleSubmit=(event)=>{
            event.preventDefault()
            let book={
                title,author,description
            }
            axios
      .post("https://bookappbackend-gh0a.onrender.com"+'/add', book)
      .then((res) => {
        alert(res.data.message)
      });
            
    }
    return(<div class="CreateBook">
    <div class="container">
      <div class="row">
        <div class="col-md-8 m-auto">
          <br /><a class="btn btn-info float-left" href="/">Show BooK List</a>
        </div>
        <div class="col-md-8 m-auto">
          <h1 class="display-4 text-center">Add Book</h1>
          <p class="lead text-center">Create new book</p>
          <form novalidate="" onSubmit={handleSubmit}>
            <div class="form-group">
              <input
                type="text"
                placeholder="Title of the Book"
                name="title"
                class="form-control"
                onChange={(e)=>setTitle(e.target.value)}
                value={title}
                spellcheck="false"
                data-ms-editor="true"
              />
            </div>
            <div class="form-group">
              <input
                type="text"
                placeholder="Author"
                name="author"
                onChange={(e)=>setAuthor(e.target.value)}
                class="form-control"
                value={author}
                spellcheck="false"
                data-ms-editor="true"
              />
            </div>
            <div class="form-group">
              <input
                type="text"
                placeholder="Describe this book"
                name="description"
                class="form-control"
                onChange={(e)=>setDescription(e.target.value)}
                value={description}
                spellcheck="false"
                data-ms-editor="true"
              />
            </div>
            <input type="submit" class="btn btn-info btn-block mt-4" />
          </form>
        </div>
      </div>
    </div>
  </div>)
}